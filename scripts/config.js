
var require = {
    baseUrl: '/scripts/lib',
    shim: {
        microevent: {
            exports: 'MicroEvent'
        },
        paper: {
            exports: 'paper'
        },
        parseuri: {
            exports: 'parseUri'
        }
    },
    paths: {
        app: '/scripts/app',
        jquery: [ 'jquery-3.1.1.min' ],
        underscore: [ 'underscore-min' ],
        bluebird: ['bluebird.min'],
        paper: ['paper-core.min']
    }
};
