
require([
    'paper',
    'domReady'
], function(paper) {
    const theta = 15;
    const Path = paper.Path;
    const Point = paper.Point;

    function mod(n, b) {
        return Math.floor((n % b + b) % b);
    }

    function opposite(angle) {
        return Math.floor(mod(angle + 12, 24));
    }

    function orientation(n, segment) {
        segment = segment || 0;
        let angle = n.segments[mod(segment + 1, n.segments.length)].point.subtract(n.segments[segment].point).angle;
        let d = mod(angle, 360);
        return Math.round(d / theta);
    }

    function rotate(n, a) {
        n.rotate(a * theta, n.segments[0].point)
    }

    function nautilus({
        position,
        fillColor,
        strokeColor
    }) {
        let d = 24;
        const angles = [1, 11, 11, 11, 7, 4, 13, 13, 13];

        position = position || paper.view.bounds.center;

        const path = new Path();
        let point = new Point(position),
            angle = 0;

        path.add(point);
        for (const a of angles) {
            angle += (12 - a);
            if (angle > 24) {
                angle -= 24;
            }
            point = point.add({
                length: d,
                angle: mod(theta * angle, 360)
            });
            path.add(point);
        }
        path.closed = true;
        if (strokeColor) {
            path.strokeColor = strokeColor;
        }
        if (fillColor) {
            path.fillColor = fillColor;
        }
//        path.fillColor = new paper.Color(Math.random(), Math.random(), Math.random());
//        path.strokeColor = new paper.Color(Math.random(), Math.random(), Math.random());
        return path;
    }

    function fill(n1, n2, { fillColor, strokeColor }) {
        let desiredOrientation = orientation(n1, 5);
        let point = n1.segments[5].point;
        fill2(point, orientation(n1, 5), opposite(orientation(n2, 3)), {
            fillColor: fillColor,
            strokeColor: strokeColor
        });
    }

    const platforms = [];
    function fill2(point, o1, o2, {
        fillColor,
        strokeColor
    }) {
        console.log('filling at ', point.x, ',', point.y);
        let desiredOrientation = o1;
        while (desiredOrientation != o2) {
            let n = nautilus({
                position: point,
                fillColor: fillColor,
                strokeColor
            });
            rotate(n, desiredOrientation - orientation(n));
            desiredOrientation = opposite(orientation(n, 8))
            platforms.push({
                point: n.segments[4].point,
                orientation: orientation(n, 4)
            });
        }
    }


    const arcs = [];
    function expand({
        fillColor,
        strokeColor
    }) {
        for (const platform of platforms) {
            const n = nautilus({
                fillColor: fillColor,
                strokeColor: strokeColor
            });
            n.scale(-1, 1);
            rotate(n, platform.orientation - orientation(n, 4));
            n.position = n.position.add(
                platform.point.subtract(n.segments[4].point)
            );
            arcs.push(n);
        }
        platforms.length = 0;
    }

    function w({
        fillColor,
        strokeColor
    }) {
        for (let i = 0; i < 4; i++) {
            const n = nautilus({
                fillColor: fillColor,
                strokeColor: strokeColor
            });
            n.scale(-1, 1);
            rotate(n, opposite(orientation(arcs[0], 3 - i) - orientation(n, 4)))
            n.position = n.position.add(
                arcs[0].segments[4 - i].point.subtract(n.segments[4].point)
            );
            arcs.push(n);
        }
    }

    function expand2({
        fillColor,
        strokeColor
    }) {
        const stop = arcs.length - 1;
        for (let i = 0; i < stop; i++) {
            const a = arcs[i];
            const b = arcs[i + 1];
            fill(a, b, { fillColor: fillColor, strokeColor: strokeColor });
        }
        const arc = arcs[arcs.length - 1];
        arcs.length = 0;
        arcs.push(arc);
    }

    function main() {
        const color1 = 'green',
              color2 = 'black';

        const color3 =  'blue',
              color4 = 'white';

        const canvas = document.getElementById('canvas');
        paper.setup(canvas);

        const n0 = nautilus({
            position: paper.view.bounds.center,
            fillColor: 'red',
            strokeColor: color1
        });
        const spikes = [];
        for (let i = 0; i < 4; i++) {
            let n = nautilus({
                fillColor: 'red',
                strokeColor: color2
            });
            n.scale(1, -1);
            let delta = orientation(n0, i) - orientation(n, 4);
            rotate(n, delta);
            n.position = n.position.add(
                n0.segments[i].point.subtract(n.segments[4].point)
            );
            spikes.push(n);
        }

        for (let i = 0; i < 4; i++) {
            let s = spikes[3];
            platforms.push({
                point: s.segments[9 - i].point,
                orientation: opposite(orientation(s, 8 - i))
            });
        }

        platforms.push({
            point: n0.segments[4].point,
            orientation: orientation(n0, 4)
        })

        fill2(
            paper.view.bounds.center,
            opposite(orientation(n0, 8)),
            opposite(orientation(spikes[0], 3)),
            {
                fillColor: 'red',
                strokeColor: color4
            }
        );

        for (let i = 0; i < 3; i++) {
            fill(spikes[i], spikes[i + 1], {
                fillColor: color3,
                strokeColor: color4
            });
        }

        expand({
            fillColor: 'green',
            strokeColor: color2
        });

        w({
            fillColor: 'yellow',
            strokeColor: color2
        });
        expand2({
            fillColor: 'blue',
            strokeColor: color4,
        });

        for (let i = 0; i < 12; i++) {
            expand({
                fillColor: color1,
                strokeColor: color2
            });
            expand2({
                fillColor: color3,
                strokeColor: color4,
            });
        }
    }

    main();

});
