
define([], function() {

    function fullscreen() {
        var canvas = document.getElementById("canvas");
        canvas.width  = window.innerWidth;
        canvas.height = window.innerHeight;
        canvas.addEventListener('mousedown', function(event) {
            console.log(this);
            if(this.webkitRequestFullScreen) {
                this.webkitRequestFullScreen();
            }
            else {
                this.mozRequestFullScreen();
            }
        });
    };

    return fullscreen;
});
