
define(function() {
    let onResize = function(cb) {
        window.addEventListener('resize', resizeThrottler, false);
        var resizeTimeout;
        function resizeThrottler() {
            if ( !resizeTimeout ) {
                resizeTimeout = setTimeout(function() {
                    resizeTimeout = null;
                    cb();
                }, 66);
            }
        }
    };

    return onResize;
});
