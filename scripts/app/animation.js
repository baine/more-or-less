
define(['paper'], function(paper) {
    const Point = paper.Point;
    const diff = 1 / 12;
    const symbolCache = {};
    function makeSymbol(x) {
        if (!symbolCache[x]) {
            var svg = $('<svg xmlns="http://www.w3.org/2000/svg">');
            $('<text>')
                .attr('font-size', 300).attr('font-family', 'Georgia')
                .html(x).appendTo(svg);
            symbolCache[x] = new paper.Symbol(paper.project.importSVG(svg[0]));
        }
        return symbolCache[x];
    }

    function getStartPoint(r) {
        var relation = r.relation;
        if (relation === '<') {
            return paper.view.bounds.leftCenter.add(new Point(
                -1 * r.symbolItem.bounds.width,
                diff * r.symbolItem.bounds.height
            ));
        } else if (relation === '>') {
            return paper.view.bounds.rightCenter.add(new Point(
                r.symbolItem.bounds.width,
                diff * r.symbolItem.bounds.height
            ));
        } else if (relation === '=') {
            return paper.view.bounds.bottomCenter.add(new Point(
                0,
                (1 + diff)* r.symbolItem.bounds.height
            ));
        } else {
            throw new Error('unrecognized relation ', relation);
        }
    }

    function getFailPoint(r) {
        var relation = r.relation;
        if (relation === '>') {
            return paper.view.bounds.leftCenter.add(new Point(
                -1 * r.symbolItem.bounds.width,
                diff * r.symbolItem.bounds.height
            ));
        } else if (relation === '<') {
            return paper.view.bounds.rightCenter.add(new Point(
                r.symbolItem.bounds.width,
                diff * r.symbolItem.bounds.height
            ));
        } else if (relation === '=') {
            return paper.view.bounds.topCenter.subtract(new Point(
                0,
                (1 + diff)* r.symbolItem.bounds.height
            ));
        } else {
            throw new Error('unrecognized relation ', relation);
        }
    }

    const center = function(r) {
        return paper.view.bounds.center.add(new Point(
            0,
            diff * r.symbolItem.bounds.height
        ));
    }

    const R = function(relation, success) {
        var r = this;
        this.relation = relation;
        this.symbolItem = makeSymbol(relation).place(new Point(0, 0));
        this.symbolItem.position = getStartPoint(this);
        if (success) {
            this.paths = [{
                finish: function() {
                    return center(r)
                },
                duration: 0.1,
                complete: 0.0
            }, {
                finish: function() {
                    return center(r)
                },
                duration: 1.0,
                complete: 0.0
            }];
        } else {
            this.paths = [{
                finish: function() {
                    return getFailPoint(r);
                },
                duration: 0.2,
                complete: 0.0
            }];
        }
    };

    R.prototype.rescale = function(s) {
        this.symbolItem.scale(s);
    }

    R.prototype.isComplete = function() {
        return this.complete;
    }

    R.prototype.tick = function(delta) {
        this.complete = true;
        let start = getStartPoint(this);
        for (const path of this.paths) {
            if (delta === 0) {
                break;
            }
            if (path.complete < path.duration) {
                this.complete = false;
                path.complete += delta;
                if (path.complete > path.duration) {
                    delta = path.complete - path.duration;
                } else {
                    delta = 0;
                }
                var destination = start
                        .add( (path.finish().subtract(start)).multiply(path.complete / path.duration));
                this.symbolItem.position = destination;
            } else {
                start = path.finish();
            }
        }
        if (this.complete) {
            this.symbolItem.remove();
        }
    }
    return R;
});
