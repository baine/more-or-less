
define(['paper', 'app/bit', 'microevent'], function(paper, Bit, MicroEvent) {

    function TenBits(options={}) {
        let self = this;
        this._bits = [];
        let mask = [];
        let count = 0;
        let value = 0;
        for (let i = 0; i < 10; i++) {
            mask.push(false);
            let bit = new Bit();
            this._bits.push(bit);
            bit.bind('flip', function(c) {
                return function(n) {
                    if (n) {
                        value = value + 1;
                        mask[c] = true;
                    } else {
                        value = value - 1;
                        mask[c] = false;
                    }
                    self.trigger('number', value);
                    self.trigger('mask', mask);
                };
            }(count));
        }
        this.bounds = options.bounds || paper.view.bounds;
    }

    TenBits.prototype.flip = function(i) {
        this._bits[i].flip();
    };

    Object.defineProperty(TenBits.prototype, 'bounds', {
        get: function() { return this._bounds; },
        set: function(bounds) {
            this._bounds = bounds;

            let radius = Math.min(bounds.width / 15, bounds.height / 7.5);
            let leftMargin = (bounds.width - (15 * radius)) / 2.0;
            let topMargin = (bounds.height - (7.5 * radius)) / 2.0;
            let count = 0;
            for (let i = 0; i < 2; i++) {
                for (let j = 0; j < 5; j++) {
                    let center = bounds.topLeft.add(
                        new paper.Point(leftMargin + 2.5 * (j + 1) * radius,
                                        topMargin + 2.5 * (i + 1) * radius));
                    let bit = this._bits[count++];
                    bit.center = center;
                    bit.radius = radius;
                }
            }
        }
    });

    Object.defineProperty(TenBits.prototype, 'editable', {
        get: function() { return this._editable; },
        set: function(e) {
            e = e ? true : false;
            this._editable = e;
            for (let b of this._bits) {
                b.editable = e;
            }
        }
    });

    MicroEvent.mixin(TenBits);
    return TenBits;
});
