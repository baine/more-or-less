
define([
    'howler.min',
    'bluebird',
    'paper'
], function(
    howler,
    Promise,
    paper) {

    var ping = new howler.Howl({
        src: ['/sounds/ping.wav']
    });

    var Scoreboard = function() {
        this.score = 0;
        this.max = 3;

        var radius = 20;
        var outerCircles = [];
        var masks = [];
        var groups = [];
        this.innerCircles = [];
        this.reveals = [0.0, 0.0, 0.0];

        for (var i = 0; i < 3; i++) {
            var centerPoint = new paper.Point(0, 0);
            var circle = new paper.Path.Circle(centerPoint, radius);
            circle.strokeColor = 'black';
            outerCircles.push(circle);
            var centerCircle = new paper.Path.Circle(centerPoint, radius * .5);
            centerCircle.fillColor = 'black';
//            centerCircle.selected = true;
            this.innerCircles.push(centerCircle);
            var d = radius * .7
            var mask = new paper.Path.Rectangle(centerPoint.add(new paper.Point(-1 * d, -1 * d)), centerPoint.add(new paper.Point(d, d)));
            masks.push(mask);
//            mask.selected = true;
            var group = new paper.Group(mask, centerCircle);
            group.clipped = true;
            group.opacity=0.99;
            groups.push(group);
        }
        this.masks = masks;
        this.groups = groups;
        this.outerCircles = outerCircles;
    };

    Scoreboard.prototype.increment = function() {
        ping.play();
        this.score++;
    }

    Scoreboard.prototype.placeAtop = function(rectangle) {
        for (var i = 0; i < this.groups.length; i++) {
            var c = this.outerCircles[i];
            var g = this.groups[i];
            c2 = this.innerCircles[i];
            for (var x of [c, g, c2]) {
                x.scale((rectangle.width / 14) / g.bounds.width);
                x.position = rectangle.topCenter.add(new paper.Point(2 * (i - 1) * c.bounds.width, c.bounds.height * 2))
            }
        }
        placeMasks(this);
    }

    var placeMasks = function(scoreboard) {
        for (var s = 0; s < scoreboard.masks.length; s++) {
            scoreboard.masks[s].position.y = scoreboard.outerCircles[s].position.y
                + scoreboard.masks[s].bounds.height * scoreboard.reveals[s];
        }
    }

    Scoreboard.prototype.getScore = function() {
        return this.score;
    }

    Scoreboard.prototype.reset = function() {
        this.score = 0;
    }

    Scoreboard.prototype.tick = function(delta) {
        var velocity = 4;
        for (var i = 0; i < this.max; i++) {
            if (i < this.score) {
                if (this.reveals[i] < 1.0) {
                    this.reveals[i] += (velocity * delta);
                    if (this.reveals[i] > 1.0) {
                        this.reveals[i] = 1.0;
                    }
                }
            } else {
                if (this.reveals[i] > 0.0) {
                    this.reveals[i] -= (velocity * delta);
                    if (this.reveals[i] < 0.0) {
                        this.reveals[i] = 0.0;
                    }
                }
            }
        }
        placeMasks(this);
    }

    return Scoreboard;
});
