
define(function() {
    function fibs(max) {
        let a = 1,
            b = 1,
            result = [];
        while (a < max) {
            let c = a + b;
            result.push(a);
            a = b;
            b = c;
        }
        return result;
    }

    const lessons = [
        {
            lhs: _.range(0, 11, 1),
            rhs: _.range(0, 11, 1)
        },
        {
            lhs: _.range(10, 21, 1),
            rhs: _.range(10, 21, 1)
        },
        {
            lhs: [9, 10, 11],
            rhs: [9, 10, 11]
        },
        {
            lhs: _.range(20, 31, 1),
            rhs: _.range(20, 31, 1)
        },
        {
            lhs: _.range(0, 22, 3),
            rhs: _.range(0, 22, 7)
        },
        {
            lhs: _.range(101, 1000, 101),
            rhs: _.range(101, 1000, 101),
        },
        {
            lhs: _.range(0, 100, 10),
            rhs: _.range(0, 100, 10),
        },
        {
            lhs: _.range(0, 50, 5),
            rhs: _.range(0, 50, 5)
        },
        {
            lhs: _.range(0, 1000, 100),
            rhs: _.range(0, 1000, 100)
        },
        {
            lhs: [99, 100, 101],
            rhs: [99, 100, 101]
        },
        {
            lhs: [1, 10, 100, 1000, 10000],
            rhs: [1, 10, 100, 1000, 10000]
        },
        {
            lhs: [2, 20, 200, 2000, 20000],
            rhs: [2, 20, 200, 2000, 20000],
        },
        {
            lhs: _.range(0, 10000, 1000),
            rhs: _.range(0, 10000, 1000)
        },
        {
            lhs: fibs(1000),
            rhs: fibs(1000),
        }

    ];
    return {
        lessons: lessons,
        fibs: fibs
    }
})
