
define([
    'bluebird',
    'howler.min'
], function(
    Promise,
    howler
) {
    function promise(h) {
        h.play();
        return new Promise(function(resolve, reject) {
            h.once('end', resolve);
        });
    }

    const applause = new howler.Howl({
        src: ['/sounds/applause-2.wav']
    });

    const bonks = [1, 2, 3].map(
        (n) =>
            new howler.Howl({
                src: [`/sounds/bonk-${n}.wav`],
            })
    );

    const f = new howler.Howl({
        src: ['/sounds/flip.wav']
    });

    return {
        applaud: function() {
            return promise(applause);
        },
        bonk: function() {
            const b = bonks[Math.floor(Math.random() * bonks.length)];
            return promise(b);
        },
        flip: () => promise(f)
    };

});
