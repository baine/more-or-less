
define(['paper', 'microevent'], function(paper, MicroEvent) {

    function StartButton(options={}) {
        let bounds = options.bounds || paper.view.bounds;
        let button = new paper.Path.Circle(bounds.center,
                                           Math.min(bounds.width, bounds.height)  / 6);
        button.fillColor = 'green';
        let started = false;
        let self = this;
        button.onMouseDown = function() {
            if (!started) {
                button.remove();
                started = true;
                setTimeout(() => self.trigger('start'));
            }
        };
    }

    MicroEvent.mixin(StartButton);
    return StartButton;
});
