
define(['paper', 'app/bit', 'microevent'], function(paper, Bit, MicroEvent) {

    function NumberBoard(options={}) {
        let bounds = options.bounds || paper.view.bounds;
        let mask = options.mask;
        if (!mask) {
            mask = [];
            for (let i = 0; i < 10; i++) {
                if (options.n && i < options.n) {
                    mask.push(true);
                } else {
                    mask.push(false);
                }
            }
        }

        let radius = Math.min(bounds.width / 15, bounds.height / 7.5);
        let self = this;

        let leftMargin = (bounds.width - (15 * radius)) / 2.0;
        let topMargin = (bounds.height - (7.5 * radius)) / 2.0;

        self._bits = [];
        self.value = 0;
        self.mask = [];
        for (let i = 0; i < 10; i++) {
            self.mask[i] = false;
        }

        let count = 0;
        for (let i = 0; i < 2; i++) {
            for (let j = 0; j < 5; j++) {
                let center = bounds.topLeft.add(
                    new paper.Point(leftMargin + 2.5 * (j + 1) * radius,
                                    topMargin + 2.5 * (i + 1) * radius));
                let bit = new Bit(center, radius);
                bit.bind('flip', function(c) {
                    return function(n) {
                        if (n) {
                            self.value = self.value + 1;
                            self.mask[c] = true;
                        } else {
                            self.value = self.value - 1;
                            self.mask[c] = false;
                        }
                        self.trigger('number', self.value);
                        self.trigger('mask', self.mask);
                    };
                }(count));
                self._bits.push(bit);
                if (mask[count++]) {
                    bit.flip();
                }
            }
        }
    }

    NumberBoard.prototype.flip = function(i) {
        this._bits[i].flip();
    };

    Object.defineProperty(NumberBoard.prototype, 'editable', {
        get: function() { return this._editable; },
        set: function(e) {
            e = e ? true : false;
            this._editable = e;
            for (let b of this._bits) {
                b.editable = e;
            }
        }
    });

    MicroEvent.mixin(NumberBoard);
    return NumberBoard;
});
