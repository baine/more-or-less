
define([
    'bluebird',
    'paper'
], function(
    Promise,
    paper
) {

    const symbolCache = {};
    function makeSymbol(x) {
        if (!symbolCache[x]) {
            var svg = $('<svg xmlns="http://www.w3.org/2000/svg">');
            $('<text>')
                .attr('font-size', 300).attr('font-family', 'Georgia')
                .html(x).appendTo(svg);
            symbolCache[x] = new paper.Symbol(paper.project.importSVG(svg[0]));
        }
        return symbolCache[x];
    }

    function angleBetweenPoints(a, b) {
        return Math.atan2(a.y - b.y, a.x - b.x);
    }

    const H = function(letter) {
        this.letter = letter;
        console.log('letter: ', this.letter);
    }

    function mod(x, b) {
        return (x % b + b) % b;
    }

    H.prototype.play = function() {
        const letter = this.letter;
        return new Promise(function(resolve, reject) {

            const birds = [];
            for (let i = 0; i < 12; i++) {
                for (let j = 0; j < 12; j++) {
                    let x = (j + 0.5)  / 12 * paper.view.bounds.width;
                    let y = (i + 0.5) / 12 * paper.view.bounds.height;
                    let circle = makeSymbol(letter).place(paper.view.bounds.center);
                    circle.scale(10 / circle.bounds.width);
                    birds.push({
                        body: circle,
                        perch: new paper.Point(x, y)
                    });
                }
            }

            let state = 'explode';

            const frameHandler = function(event) {
                let perchRemaining = 1000;
                const velocity = 400;
                const dx = event.delta * velocity;
                switch(state) {
                case 'explode':
                    let next = 'perch-1';
                    for (const bird of birds) {
                        if (bird.body.position.getDistance(bird.perch) > 0.001) {
                            next = 'explode';
                            const q = bird.perch.subtract(bird.body.position).normalize(dx);
                            let p = bird.body.position.add(q);
                            if (p.getDistance(paper.view.bounds.center) > bird.perch.getDistance(paper.view.bounds.center)) {
                                p = bird.perch;
                            }
                            bird.body.position = p;
                        }
                    }
                    state = next;
                    break;
                case 'perch-1':
                    for (const bird of birds) {
                        bird.perchRemaining = (500 + Math.random() * 1000) / 1000;
                    }
                    state = 'spiral-1';
                    break;
                case 'spiral-1':
                    let next2 = 'perch-2';
                    for (const bird of birds) {
                        if (bird.perchRemaining > 0) {
                            bird.perchRemaining -= event.delta;
                            next2 = 'spiral-1';
                        } else {
                            let d = bird.body.position.getDistance(paper.view.bounds.center);
                            d = (d - velocity * event.delta);
                            if (d < 0) {
                                d = 0;
                            }
                            let theta = angleBetweenPoints(bird.body.position, paper.view.bounds.center);
                            theta = theta + 3 * event.delta;
                            let newPosition = paper.view.bounds.center.add(
                                new paper.Point(d * Math.cos(theta), d * Math.sin(theta)));
                            if (newPosition.getDistance(paper.view.bounds.center) < 11) {
                                newPosition = paper.view.bounds.center;
                                bird.body.visible = false;
                            } else {
                                next2 = 'spiral-1';
                            }
                            bird.body.position = newPosition;
                        }
                    }
                    state = next2;
                    break;
                case 'perch-2':
                    birds[0].body.visible = true;
                    birds[0].body.scale(1.1);
                    if (birds[0].body.bounds.width > 100) {
                        state = 'done';
                    }
                    break;
                case 'done':
                    off();
                    state = 'truly-done';
                    resolve();
                    break;
                case 'truly-done':
                    console.log('im surprised were here');
                }
            };

            paper.view.on({
                frame: frameHandler
            });
            function off() {
                paper.view.off({
                    frame: frameHandler
                });
            };
        });
    }
    return H;
});
