define(['paper'], function(paper) {

    const Path = paper.Path;
    const Point = paper.Point;

    function makeReel({numbers, anchor}) {
        var font = 'Georgia';
        var height = 300;
        var offset = 60;
        var svg = $(`<svg id="reel" xmlns="http://www.w3.org/2000/svg">`);
        var g = $('<g>').appendTo(svg);
        for (var i = 0; i < (numbers.length + 1); i++) {
            var number = numbers[Math.floor((numbers.length - i) % numbers.length)];
            $('<text text-anchor="end">')
                .attr('text-anchor', anchor)
                .attr('y', i * (height + offset))
                .attr('font-size', height).attr('font-family', font)
                .html(number)
                .appendTo(g);
        }
        return svg[0];
    }

    var Spinner = function(anchor) {
        this.anchor = anchor
    }
    Spinner.prototype.setNumbers = function(numbers) {
        console.log('setting numbers');
        this.numbers = numbers;
        var reelSymbol = new paper.Symbol(paper.project.importSVG(makeReel({
            numbers: this.numbers,
            anchor: this.anchor
        })));

        if (this.reelSymbolItem != null) {
            this.reelSymbolItem.remove();
            this.maskItem.remove();
            this.group.remove();
        }
        this.reelSymbolItem = reelSymbol.place(0, 0);
        this.maskItem = new Path.Rectangle(this.reelSymbolItem.bounds.scale(1, 1 / (this.numbers.length + 1)));
        var group = new paper.Group(this.maskItem, this.reelSymbolItem);
        group.clipped = true;
        this.group = group;
        set(this, 0.0);
    }

    Spinner.prototype.placeWithin = function(rectangle) {
        if (this.reelSymbolItem != null) {
            var hScale =  rectangle.width /this.maskItem.bounds.width;
            var vScale = rectangle.height / this.maskItem.bounds.height;
            var scale = Math.min(hScale, vScale);
            this.group.scale(scale);
            this.scale = scale;
            this.maskItem.position = rectangle.center;
            set(this);
        }
    }

    var set = function(spinner, n) {
        if (n == undefined) {
            n = spinner.value;
        }
        if (n < 0 || n > spinner.numbers.length) {
            throw new Error('invalid value: ' + n);
        }
        spinner.value = n;

        var c = 1 / 24;
        spinner.reelSymbolItem.position = spinner.maskItem.position.add(new Point(
            0,
            (c + (spinner.value) - spinner.numbers.length / 2) * spinner.maskItem.bounds.height
        ));
    }

    Spinner.prototype.getMod = function() {
        return this.getDistance();
    }

    Spinner.prototype.getDistance = function() {
        var v = this.value % 1;
        if (v > 0.5) {
            return -1  + v;
        } else {
            return v;
        }
    }

    Spinner.prototype.getNumber = function() {
        var index = Math.floor((this.value + 0.5) % this.numbers.length);
        var n = this.numbers[index];
        return n;
    }

    Spinner.prototype.getScale = function() {
        return this.scale;
    }

    Spinner.prototype.inc = function(delta) {
        var v = this.value + delta;
        while (v >= this.numbers.length) {
            v = v - this.numbers.length;
        }
        while (v < 0.0) {
            v = v + this.numbers.length;
        }
        set(this, v);
    }

    return Spinner

});
