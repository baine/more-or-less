
define(function() {

    var State = function({name, transitions, enterCallback, exitCallback}) {
        if (!name || !transitions) {
            throw new Error('you must specify a name and transitions');
        }
        this.name = name;
        this.transitions = transitions;
        this.enterCallback = enterCallback,
        this.exitCallback = exitCallback;
    }

    State.prototype.enter = function() {
        console.log('entering state ' + this.name);
        if (this.enterCallback) {
            this.enterCallback();
        }
    }

    State.prototype.exit = function() {
        console.log('leaving state ' + this.name);
        if (this.exitCallback) {
            this.exitCallback()
        }
    }

    var FSM = function(states, start) {
        if (!states || !start) {
            throw new Error('please pass a states and a start state');
        }
        this.states = {};
        for (var s of states) {
            if (this.states.hasOwnProperty(s.name)) {
                throw new Error('duplicate state: ' + s.name);
            }
            this.states[s.name] = s;
        }

        this.state = this.states[start];
    }
    FSM.prototype.getState = function() {
        return this.state.name;
    }
    FSM.prototype.signal = function(signal) {
        var transition = this.state.transitions[signal];
        if (!transition) {
            throw new Error('no transition for signal ' + signal)
        }
        var nextState = transition.next;
        if (!nextState) {
            throw new Error('transition for signal ' + signal + ' does not include the next state')
        }
        var stateChange = (nextState !== this.state.name);

        if (stateChange) {
            this.state.exit();
        }
        this.state = this.states[nextState];
        if (!this.state) {
            throw new Error('no such state: ' + nextState);
        }

        if (transition.action) {
            transition.action();
        }
        if (stateChange) {
            this.state.enter();
        }
    }

    return {
        FSM: FSM,
        State: State,
    }
});
