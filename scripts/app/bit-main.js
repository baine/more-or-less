
require([
    'paper',
    'app/bit',
    'app/number-board',
    'app/fullscreen'
], function(paper, Bit, NumberBoard, fullscreen) {

    let init = function(event) {
        fullscreen();
        resizeCanvas();
        window.addEventListener('resize', resizeCanvas, false);

        let canvas = document.getElementById('canvas');
        paper.setup(canvas);

//        let tool = new paper.Tool();

        let bit = new Bit(new paper.Point(300, 300), 50);

        let b2 = new Bit(new paper.Point(100, 100), 50);

        let numberBoard = new NumberBoard();
        numberBoard.bind('number', function(n) {
            console.log(n);
        });

        resizeCanvas();
    };

    function resizeCanvas() {
        var canvas = document.getElementById('canvas');
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }

    if (document.readyState === 'complete') {
        init();
    } else {
        document.addEventListener('DOMContentLoaded', init);
    }
});
