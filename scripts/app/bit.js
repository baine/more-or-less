
define(
    [ 'paper', 'microevent', 'app/sounds'],
    function(paper, MicroEvent, sounds) {
        function Bit(options={}) {
            this._value = false;
            this._editable = true;
            let self = this;

            self._circle = new paper.Shape.Circle();
            self._circle.strokeColor = 'black';
            self._circle.fillColor =  this.value ? 'black' : 'white';

            let mouseHandler = function() {
                if (self.editable) {
                    self.flip();
                }
            };
            self._circle.onMouseDown = mouseHandler;
            self._circle.onMouseEnter = mouseHandler;

            self.radius = options.radius || 10;
            self.center = options.center || paper.view.bounds.center;
        }

        Bit.prototype.flip = function() {
            let self = this;
            let v = !self.value;
            setTimeout(() => self.value = v);
        };

        Object.defineProperty(Bit.prototype, 'editable', {
            set: function(x) {
                this._editable = x ? true : false;
                if (!this._editable) {
                    this._circle.strokeColor = 'grey';
                    if (this.value) {
                        this._circle.fillColor = this.value ? 'grey' : 'white';
                    }
                } else {
                    this._circle.strokeColor = 'black';
                    if (this.value) {
                        this._circle.fillColor = this.value ? 'black' : 'white';
                    }
                }
            },
            get: function() { return this._editable; }
        });


        Object.defineProperty(Bit.prototype, 'radius', {
            set: function(r) {
                this._circle.radius = r;
            },
            get: function() {
                return this._circle.radius;
            }
        });

        Object.defineProperty(Bit.prototype, 'center', {
            set: function(c) {
                this._circle.position = c;
            },
            get: function() {
                return this._circle.position;
            }
        });

        Object.defineProperty(Bit.prototype, 'value', {
            set: function(x) {
                x = x ? true : false;
                if (x ? !this._value : this.value) {
                    this._value = !this._value;
                    this.trigger('flip', this._value);
                    if (this._value) {
                        this.trigger('one');
                        this._circle.fillColor = this.editable ? 'black' : 'grey';
                    } else {
                        this.trigger('zero');
                        this._circle.fillColor = 'white';
                    }
                    sounds.flip();
                }
            },
            get: function() {
                return this._value ? true : false;
            }
        });


        MicroEvent.mixin(Bit);
        return Bit;

    });
