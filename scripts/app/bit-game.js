require([
    'paper',
    'app/bit',
    'app/ten-bits',
    'app/start-button',
    'app/resize'
], function(paper, Bit, NumberBoard, StartButton, onResize) {


    let state = null;

    function draw() {
        if (state) {
            state.draw();
        }
    }

    onResize(function() {
        console.log('resized baby');
        draw();
    });

    let random = function() {
        return Math.floor(Math.random() * 1024);
    };

    let mask = function(n) {
        let m = [];
        for (var i = 0; i < 10; i++) {
            m.push(n & (1 << i) ? true : false);
        }
        return m;
    };

    let start = function() {
        let canvas = document.getElementById('canvas');
        if(canvas.webkitRequestFullScreen) {
            canvas.webkitRequestFullScreen();
        }
        else {
            canvas.mozRequestFullScreen();
        }

        let n1 = random();
        let n2 = random();
        while (n2 == n1) {
            n2 = random();
        }

        let bounds = paper.view.bounds;
        let leftHalf = new paper.Rectangle(bounds.topLeft, bounds.bottomCenter);
        let rightHalf = new paper.Rectangle(bounds.topCenter, bounds.bottomRight);
        let numberBoard = new NumberBoard({ bounds: leftHalf, mask: mask(n1) });
        let numberBoard2 = new NumberBoard({ bounds: rightHalf, mask: mask(n2) });

        state = {
            draw: function() {
                let canvas = document.getElementById('canvas');
                canvas.width = document.body.clientWidth; //document.width is obsolete
                canvas.height = document.body.clientHeight; //document.height is obsolete
                let bounds = paper.view.bounds;
                numberBoard.bounds = new paper.Rectangle(bounds.topLeft, bounds.bottomCenter);
                numberBoard2.bounds = new paper.Rectangle(bounds.topCenter, bounds.bottomRight);
            }
        };

        let m1 = mask(n1);
        for (var i = 0; i < 10; i++) {
            if (m1[i]) {
                numberBoard.flip(i);
            }
        }

        let m2 = mask(n2);
        for (var i = 0; i < 10; i++) {
            if (m2[i]) {
                numberBoard2.flip(i);
            }
        }

        numberBoard.editable = false;
        numberBoard.bind('number', function(n) {
            console.log(n);
        });
        numberBoard2.bind('number', function(n) {
            console.log('good guess');
        });

        numberBoard2.bind('mask', function(m) {
            console.log(m);
        });
    };

    let init = function() {
        let canvas = document.getElementById('canvas');
        paper.setup(canvas);

        let sb = new StartButton();
        sb.bind('start', function() {
            start();
        });

    };

    init();
});
