
require([
    'jquery',
    'app/harper-animation'
], function(
    $,
    HarperAnimation
) {
$(function() {
    paper.setup(canvas);
    new HarperAnimation().play();
});
});
