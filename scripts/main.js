
require([
    'jquery',
    'hammer.min',
    'underscore',
    'bluebird',
    'paper',
    'parseuri',
    'app/spinner',
    'app/sounds',
    'app/scoreboard',
    'app/animation',
    'app/lessons',
    'app/harper-animation'
], function(
    $,
    Hammer,
    _,
    Promise,
    paper,
    parseUri,
    Spinner,
    sounds,
    Scoreboard,
    RelationAnimation,
    lessons,
    HarperAnimation
) {

    var globals = {
        scoreboard: null,
        animations: [],
        lhsSpinner: {
            state: 'speedup',
            v: 0,
            a: 0,
            max: 15.732,
            acceleration: 15.3427,
            deceleration: 3.82345,
            reel: null
        },
        rhsSpinner: {
            state: 'speedup',
            v: 0,
            a: 0,
            max: 16.87,
            acceleration: 32,
            deceleration: 2.9,
            reel: null
        }
    };
    function initGlobals() {
        const parsed = parseUri(window.location.href);
        globals.debug = parsed.queryKey.debug === 'true';
        globals.message = parsed.queryKey.message || 'I-LOVE-YOU-HARPER';

        globals.level = 0;
        if (parsed.queryKey.lesson) {
            globals.level = parseInt(parsed.queryKey.lesson);
        }
        globals.lhsSpinner.reel = new Spinner('end')
        globals.rhsSpinner.reel = new Spinner('start');
        resetNumbers();

        globals.scoreboard = new Scoreboard();
        globals.scoreboard.reset();
    }

    function resetNumbers() {
        const level = globals.level;
        const l = lessons.lessons;
        let lhs = null;
        let rhs = null;
        if (level < l.length) {
            lhs = l[level].lhs;
            rhs = l[level].rhs;
        } else {
            let f = function(n) {
                let result = [];
                let x = 0;
                for (let i = 0; i < 10; i++) {
                    x = Math.floor(x + Math.random() * n * 1000);
                    result.push(x);
                }
                return result;
            }
            lhs = f(level);
            rhs = f(level);
        }
        globals.lhsSpinner.reel.setNumbers(lhs);
        globals.rhsSpinner.reel.setNumbers(rhs);
    }

    const Point = paper.Point;

    $(function() {
        const canvas = $('#canvas')[0];
        paper.setup(canvas);

        initGlobals();

        var debugCircles = [];
        for (var i = 0; i < 13; i++) {
            var row = [];
            debugCircles.push(row);
            for (var j = 0; j < 13; j++) {
                var center = new Point(
                    paper.view.bounds.width * i / 12,
                    paper.view.bounds.height * j / 12
                );
                var circle = new paper.Path.Circle(center, 5);
                circle.selected = false;
                circle.visible = false;
                row.push(circle);
            }
        }

        function fullscreen() {
            var el = document.getElementById('canvas');
            if(el.webkitRequestFullScreen) {
                el.webkitRequestFullScreen();
            } else {
                el.mozRequestFullScreen();
            }
        }

        function resize() {
            console.log('resizing');
            placeSpinners();
            globals.scoreboard.placeAtop(paper.view.bounds.scale(1/3, 1));
            var scale = globals.lhsSpinner.reel.getScale() || 1;
            for (const animation of globals.animations) {
                animation.rescale(scale);
            }
        }

        function spin() {
            globals.lhsSpinner.state = 'speedup';
            globals.rhsSpinner.state = 'speedup';
        }

        $('#canvas').on('click', function() {
            fullscreen();
        });

        placeSpinners();

        var drawSpinner = function(spinner, delta) {

            switch(spinner.state) {
            case 'stopped':
                spinner.a = 0;
                spinner.v = 0;
                break;
            case 'speedup':
                spinner.a = spinner.acceleration;
                if (spinner.v >= spinner.max) {
                    spinner.state = 'glide';
                    spinner.glidesRemaining = Math.floor(51 * Math.random());
                }
                break;
            case 'glide':
                spinner.a = 0;
                if (spinner.glidesRemaining <= 0) {
                    spinner.state = 'slowdown';
                } else {
                    spinner.glidesRemaining--;
                }
                break;
            case 'slowdown':
                spinner.a = -1 * spinner.deceleration;
                if (spinner.v <= 0.0) {
                    spinner.state = 'centering';
                    spinner.centerFrames = 10;
                    spinner.v = -2 * spinner.reel.getMod();
                    spinner.a = 0;
                }
                break;
            case 'centering':
                if (Math.abs(spinner.reel.getMod()) < 0.01) {
                    spinner.state = 'stopping';
                    spinner.v = 0;
                }
                spinner.centerFrames--;
                break;
            case 'stopping':
                spinner.v = (-1 * spinner.reel.getMod() / delta);
                spinner.state = 'stopped';
                break;
            default:
                throw new Error('spinner in unrecognized state: ' + spinner.state);
            }
            var inc = spinner.v * delta;
            spinner.reel.inc(spinner.v * delta);
            spinner.v += spinner.a * delta;
            if (spinner.v > spinner.max) {
                spinner.v = spinner.max;
            }

            return spinner.state;
        };

        function placeSpinners() {
            globals.lhsSpinner.reel.placeWithin(new paper.Rectangle(
                paper.view.bounds.topRight.multiply(1/3),
                paper.view.bounds.bottomLeft
            ));
            globals.rhsSpinner.reel.placeWithin(new paper.Rectangle(
                paper.view.bounds.topRight.multiply(2/3),
                paper.view.bounds.bottomRight
            ));
        }

        var isCorrect = function(relation) {
            var a = globals.lhsSpinner.reel.getNumber();
            var b = globals.rhsSpinner.reel.getNumber();
            var correct = false;

            console.log(`evaluating ${a} ${relation} ${b}`);
            console.log(`mods: ${globals.lhsSpinner.reel.getMod()} <=> ${globals.rhsSpinner.reel.getMod()}`);
            if (relation === '<') {
                correct = (a < b);
            } else if (relation === '>') {
                correct = (a > b);
            } else if (relation === '=') {
                correct = (a === b);
            }
            return correct;
        }

        paper.view.onFrame = function(event) {
            var delta = event.delta;
            drawSpinner(globals.lhsSpinner, event.delta);
            drawSpinner(globals.rhsSpinner, event.delta);
            if (globals.debug) {
                drawDebugFrame();
            }

            let animationsCompleted = false;
            for (var a of globals.animations) {
                var moved = a.tick(delta);
                if (!moved) {
                    animationsCompleted = true;
                }
            }
            if (animationsCompleted) {
                globals.animations = _(globals.animations).filter((x) => !x.isComplete());
            }
            globals.scoreboard.tick(delta);
        };

        function drawDebugFrame() {
            for (var i = 0; i < 13; i++) {
                for (var j = 0; j < 13; j++) {
                    var circle = debugCircles[i][j];
                    circle.position = new Point(
                        paper.view.bounds.width * i / 12,
                        paper.view.bounds.height * j / 12
                    );
                    circle.selected = true;
                    circle.visible = true;
                }
            }
        }

        var hammertime = new Hammer($('body')[0]);
        hammertime.get('swipe').set({ direction: Hammer.DIRECTION_ALL });

        const checker = function(relation) {
            return function(event) {
                event.preventDefault();
                const correct = isCorrect(relation);
                globals.animations.push(new RelationAnimation(relation, correct));
                if (correct) {
                    globals.scoreboard.increment();
                    if (globals.scoreboard.getScore() > 2) {
                        Promise
                            .resolve()
                            .delay(1000)
                            .then(() => {
                                sounds.applaud();
                                const layer = paper.project.activeLayer;
                                const secondLayer = new paper.Layer();
                                layer.remove();
                                console.log('created second layer which is equal to the active layer: ', secondLayer == paper.project.activeLayer);
                                return new HarperAnimation(globals.message.charAt(globals.level) || '#')
                                    .play()
                                    .delay(1000)
                                    .then(() => {
                                        paper.project.layers.push(layer);
                                        secondLayer.remove();
                                        return true;
                                    });
                            })
                            .then(() => {
                                globals.level++;
                                globals.scoreboard.reset();
                                resetNumbers();
                                resize();
                                spin();
                            })
                    } else {
                        Promise
                            .delay(500)
                            .then(spin)
                    }
                } else {
                    globals.scoreboard.reset();
                    sounds.bonk().then(() => console.log('bonk complete'));
                }
            }
        };

        hammertime.on('swipeleft', checker('>'));
        hammertime.on('swiperight', checker('<'));
        hammertime.on('swipeup', checker('='));
        hammertime.on('swipedown', function(event) { spin(); });

        window.addEventListener('resize', resize, false);
        resize();
    });
});
