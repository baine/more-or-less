
require([
    'jquery',
    'underscore',
    'bluebird',
    'paper',
    'app/spinner'
], function(
    $,
    Hammer,
    _,
    Promise,
    paper,
    Spinner) {

    const Path = paper.Path;
    const Point = paper.Point;

    $(function() {
        const canvas = $('#canvas')[0];
        const context = canvas.getContext('2d');
        paper.setup(canvas);

        var spinners = [];
        var n = 10;
        for (i = 0; i < n; i++) {
            if ((i % 2) > -1) {
                var rectangle = paper.view.bounds.scale(1.0/ n, 1.0);
                var topLeft = new Point(i * paper.view.bounds.width / n, 0);
                var path = new Path.Rectangle(topLeft, rectangle.size);
                var scaleFactor = 1.0;
                path.scale(scaleFactor);
                path.strokeColor = 'black';
                var stop = 70;
                var ns = []
                for (var j = 0; j < stop; j++) {
                    ns.push(i * j);
                }

                var spinner = new Spinner(ns, 'end');
                spinner.inc(i);

                spinner.placeWithin(path.bounds);
//                spinner.set((i + 6) % 11)
                spinners.push(spinner);
            }

        }

        paper.view.onFrame = function(event) {
            for (var i = 0; i < spinners.length; i++) {
                spinners[i].inc(event.delta * 2);
            }

        }

    });


});
