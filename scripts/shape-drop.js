
require([
    'domReady',
    'paper',
    'bluebird'
], function(
    domReady,
    paper,
    Promise
) {

    /**
     * Shuffles array in place. ES6 version
     * @param {Array} a items The array containing the items.
     */
    function shuffle(a) {
        for (let i = a.length; i; i--) {
            let j = Math.floor(Math.random() * i);
            [a[i - 1], a[j]] = [a[j], a[i - 1]];
        }
    }

    const Path = paper.Path,
        Tool = paper.Tool,
        Point = paper.Point;


    const problems = [];

    function reset() {
        problems.length = 0;
        let rectangles = partitionRectangle(paper.view.bounds, 3, 2);
        shuffle(rectangles);
        let colors = [
            'red',
            'yellow',
            'blue',
        ];
        shuffle(colors);
        let sidesArray = [0, 3, 4, 6];
        shuffle(sidesArray);
        for (let i = 0; i < 3; i++) {
            let color = colors[i],
                sides = sidesArray[i],
                target = makeTarget(sides, rectangles[i * 2], color),
                shape = makeShape(sides, rectangles[i * 2 + 1], color);
            problems.push({
                shape: shape,
                target: target
            });
        }
    }

    function radius(circle) {
        return (circle.bounds.width + circle.strokeWidth) / 2;
    }

    const  marginA = 60,
        marginB = 100;
    function makeTarget(sides,
                        bounds,
                        strokeColor) {
        let radius = Math.min(bounds.width, bounds.height) / 2 - marginA;
        let blendMode = 'multiply',
            strokeWidth = 5;
        if (sides === 0) {
            return new Path.Circle({
                center: bounds.center,
                radius: radius,
                strokeColor: strokeColor,
                strokeWidth: strokeWidth,
                fillColor: '#e9e9ff',
                blendMode: blendMode
            });
        }  else {
            return new Path.RegularPolygon({
                sides: sides,
                center: bounds.center,
                radius: radius,
                strokeWidth: strokeWidth,
                strokeColor: strokeColor,
                fillColor: '#e9e9ff',
                blendMode: blendMode
            });
        }
    }

    function makeShape(sides, bounds, fillColor) {
        let radius = Math.min(bounds.width, bounds.height) / 2 - marginB;
        if (sides === 0) {
            return new Path.Circle({
                center: bounds.center,
                radius: radius,
                fillColor: fillColor
            });
        }  else {
            return new Path.RegularPolygon({
                sides: sides,
                center: bounds.center,
                radius: radius,
                fillColor: fillColor
            });
        }
    }

    function partitionRectangle(bounds, m, n) {
        var swap = (paper.view.bounds.width < paper.view.bounds.height) ? (m > n) : (m < n);
        if (swap) {
            let c = m;
            m = n;
            n = c;
        }
        const result = [],
              width = bounds.width / m,
              height = bounds.height / n;
        for (let i = 0; i < n; i++) {
            for (let j = 0; j < m; j++) {
                let a = new Point(j * width, i * height),
                    b = new Point((j + 1) * width, (i + 1) * height),
                    rectangle = new paper.Rectangle(a, b);
                result.push(rectangle);
            }
        }
        return result;
    }

    function fullscreen() {
        var el = document.getElementById('canvas');
        if(el.webkitRequestFullScreen) {
            el.webkitRequestFullScreen();
        } else {
            el.mozRequestFullScreen();
        }
    }

    function main() {
        const canvas = document.getElementById('canvas');
        paper.setup(canvas);
        reset();

        const tool = new Tool();
        tool.distanceThreshold = 10;
        let item = null;
        tool.onMouseDown = function(event) {
            let candidate = null,
                distance = null;
            for (const problem of problems) {
                let shape = problem.shape,
                    d = event.point.getDistance(shape.position);
                if (d < radius(shape)) {
                    if (distance == null || d < distance) {
                        distance = d;
                        candidate = problem;
                    }
                }
            }
            if (candidate) {
                item = candidate;
            } else {
                item = null;
            }
        };

        tool.onMouseDrag = function(event) {
            if (item) {
                item.shape.position = item.shape.position.add(event.delta);
            }
        };

        tool.onMouseUp = function(event) {
            fullscreen();
            if (item === null) {
                return;
            }
            if (item.shape.position.getDistance(item.target.position) < (radius(item.target) - radius(item.shape))) {
                item.target.strokeWidth = 10;
                const x = item;
                Promise
                    .delay(500)
                    .then(() => {
                        x.target.remove();
                        x.shape.remove();
                    })
                    .delay(500)
                    .then(() => {
                        if (paper.project.activeLayer.children.length === 0) {
                            reset();
                        }
                    });
            }
            item = null;
        };
    }

    main();
});
