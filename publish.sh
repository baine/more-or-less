#!/bin/bash

set -e
set -x

(find . -name "*~" | xargs rm) || echo "no bad files"

for page in `find . -name "*.html"`
do
    s3cmd put -P ${page} s3://more-or-less/
done

for dir in scripts sounds stylesheets
do
    s3cmd sync -P ${dir} s3://more-or-less/
done
